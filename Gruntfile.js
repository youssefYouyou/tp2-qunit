module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    concat: {
      dist: {
        src: 'js/*.js',
        dest: 'dist/built.js'
      }
    },
    uglify: {
      build :{
        src:'dist/built.js',
        dest:'dist/built.min.js'
      },
    },
    qunit: {
      files: ['test/**/*.html']
    },
    
    shint: {
      files: ['Gruntfile.js', 'src/**/*.js', 'test/**/*.js'],
      options: {
        // options here to override JSHint defaults
        globals: {
          jQuery: true,
          console: true,
          module: true,
          document: true
        }
      }
    },
    watch: {
      scripts:{
        files:['js/*.js','tests/*.js'],
        tasks:['concat','uglify'],
        options:{
          spawn:false,
          livereload:true,
        },

      }
    /*  files: ['<%= jshint.files %>'],
      tasks: ['jshint', 'qunit']*/
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-qunit');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');

  grunt.registerTask('default', ['concat','uglify','watch','qunit']);
};